// 3.3. Implementing a Recursive Algorithm
// Problem
// You want to implement a function that will recursively traverse an array 
// and return a string of the array element values, in reverse order.

var reverseArray = function (x, indx, str) {
	return indx == 0 ? str : 
		reverseArray(x, --indx, (str+= " " + x[indx]));
}
var arr = ['apple', 'orange', 'peach', 'lime'];
var str = reverseArray(arr, arr.length, "");
console.log(str);


var arr2 = ['car','boat','sun','computer'];
str = reverseArray(arr2,arr2.length,"");
console.log(str);

function factorial(n) {
	console.log('n', n);
	return n == 1 ? 1 : n * factorial(n -1);
}
var n = 3;
console.log(factorial(n));

//incluir na ordem do array
var orderArray = function(x,indx,str) {
	return indx == x.length-1 ? str : orderArray(x, ++indx, (str += x[indx] + " "));
}
var arr = ['apple','orange','peach','lime'];
var str = orderArray(arr, -1,"");
// apple orange peach lime
console.log(str);
