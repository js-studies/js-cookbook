// 3.5. Creating a Function That Remembers Its State
// Problem
// You want to create a function that can remember data, but without having to
// use global variables and without resending the same data with 
// each function call.

function greetingMaker(greeting) {
	function addName(name) {
	return greeting + " " + name;
	}
	return addName;
}
// Now, create new partial functions
var daytimeGreeting = greetingMaker("Good Day to you");
var nightGreeting = greetingMaker("Good Evening");

// if daytime
console.log(daytimeGreeting('sd'));
// if night
console.log(nightGreeting('sdas'));

function outer (x) {
	return function(y) { return x * y; };
}