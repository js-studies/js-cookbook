var num = 255;
console.log(num.toString(16));

var octoNumber = 0255; // equivalent to 173 decimal
var hexaNumber = 0xad; // equivalent to 173 decimal

var dec = 23;
console.log(dec.toString(8));
console.log(dec.toString(16));
console.log(dec.toString(2));
