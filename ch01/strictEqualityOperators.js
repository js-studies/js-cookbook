// Extra: Loose and Strict Equality Operators
// I used loose equality (== and !=) in this section, but I use strict equality (=== and !==)
// elsewhere in the book. My use of both types of operators is not a typo.
// Some folks (Douglas Crockford being the most outspoken) consider the loose equality
// operators (== and !=) to be evil, and discourage their use. The main reason many de‐
// velopers eschew loose equality operators is that they test primitive values rather than
// the variable object, in totality, and the results of the test can be unexpected.
// For instance, the following code succeeds:

var str1 = new String('test');
if (str1 == 'test') console.log('ok')

var str2 = new String('test');

if (str1 === 'test') { }
else console.log('Not ok');


var num = 0;
var str = '0';
console.log(num == str);
console.log(num === str);
