// Problem
// You want to track the elapsed time between events.
// Solution
// Create a Date object when the first event occurs, a new Date object when the second
// event occurs, and subtract the first from the second. The difference is in milliseconds;
// to convert to seconds, divide by 1,000:

var firstDate = new Date();
setTimeout (function (){
    doEvent(firstDate);
}, 2500);

function doEvent() {
    var secondDate = new Date();
    var diff = secondDate - firstDate;
    console.log(diff);
}
