// Problem
// You want to replace all matched substrings with a new substring.
// Solution
// Use the String’s replace() method, with a regular expression:

var searchString = "Now is the time, this is the tame";
var re = /t\w{2}e/g;
var replacement = searchString.replace(re, "place");
// Now is the place, this is the place
console.log(replacement);

// The literal regular expression begins and ends with a slash ( / ). As an alternative, I could
// have used the built-in RegExp object:
var re = new RegExp('t\\w{2}e',"g");
var replacement = searchString.replace(re,"creme");

console.log(replacement);
