

var str1 = "this is a simple string"; // the quoted string is the literal
var num1 = 1.45; // the value of 1.45 is the literal
var answer = true; // the values of true and false are boolean literals

//We can create primitive boolean, string, and number variables either by using a literal
//representation or using the object without using the new operator:
var str1 = String("this is a simple string"); // primitive string
var num1 = Number(1.45); // primitive number
var bool1 = Boolean(true); // primitive boolean
//To deliberately instantiate an object, use the new operator:
var str2 = new String("this is a simple string"); // String object instance
var num2 = new Number(1.45); // Number object instance
var bool2 = new Boolean(true); // primitive boolean

var str = String("string");
var num = Number(1.45);
var bool = Boolean(true);
if (str === "string") {
    console.log('equal');
}

if (num === 1.45) {
console.log('equal');
}

if (bool === true) {
console.log('equal');
}
//all equal
//
// The primitive variables (those not created with new ) are strictly equal to the literals,
// while the object instances are not. Why are the primitive variables strictly equal to the
// literals? Because primitives are compared by value, and values are literals.

//all not equal
var str2 = new String("string");
var num2 = new Number(1.45);
var bool2 = new Boolean(true);
if (str2 === "string") {
console.log('equal');
} else {
console.log('not equal');
}

if (num2 === 1.45) {
console.log('equal');
} else {
console.log('not equal');
}

if (bool2 === true) {
console.log('equal');
} else {
console.log('not equal')
}


var num1 = 1.45;
var num2 = new Number(1.45);
// prints out number
console.log(typeof num1);
// prints out object
console.log(typeof num2);
