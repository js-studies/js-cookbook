// Problem
// You have a string with several sentences, one of which includes a list of items. The list
// begins with a colon (:) and ends with a period (.), and each item is separated by a comma.
// You want to extract just the list.

// Before:
// This is a list of items: cherries, limes, oranges, apples.

// After:
// ['cherries','limes','oranges','apples']

var sentence = 'This is one sentence. This is a sentence with a list of items:' +
'cherries, oranges, apples, bananas. That was the list of items.';

var start = sentence.indexOf(':');
var end = sentence.indexOf('.', start + 1);
var listStr = sentence.substring(start + 1, end);
console.log(listStr);
var fruits = listStr.split(',');

// CHAINING
// The example code in this recipe is correct, but a little verbose. We can compress the
// code by using JavaScript’s method chaining, allowing us to attach one method call to the
// end of a previous method call if the object and methods allow it. In this case, we can
// chain the split() method directly to the substring() method:
// var start = sentence.indexOf(":");
// var end = sentence.indexOf(".", start+1);
// var fruits = sentence.substring(start+1, end).split(",");

//CLEANUP
// The result of splitting the extracted string is an array of list items. However, the items
// come with artifacts (leading spaces) from sentence white space. In most applications,
// we’ll want to clean up the resulting array elements.

//clean up the array with forEach
// fruits.forEach(function (elmnt, indx, arry) {
//     arry[indx] = elmnt.trim();
// });

//clean up with regEx
var fruits = listStr.split(/\s*,\s*/);

console.log(fruits);
