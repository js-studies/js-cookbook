// Swapping Words in a String Using Capturing
// Parentheses
// Problem
// You want to accept an input string with first and last name, and swap the names so the
// last name is first.

var  name = "Abe Lincoln";
var re = /^(\w+)\s(\w+)$/;
var newName = name.replace(re, "$2 - $1");
console.log(newName);

var name = "Abe Lincoln";
var re = /^(\w+)\s(\w+)$/;
var result = re.exec(name);
var newName = result[2] + " " + result[1];
console.log(newName);
