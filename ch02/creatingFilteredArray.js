// Problem
// You want to filter element values in an array and assign the results to a new array.

var charSet = ["**","bb","cd","**","cc","**","dd","**"];
var newArray = charSet.filter(function (element) {
    return (element !== "**");
});
// ["bb", "cd", "cc", "dd"]
console.log(newArray);

// the method is a way of
// applying a callback function to every array element.
// to return new array like map
