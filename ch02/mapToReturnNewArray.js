// Problem
// You want to convert an array of decimal numbers into a new array
// with their hexadecimal equivalents.

var decArray = [23, 255, 122, 5, 16, 99];

var hexArray = decArray.map(function (element) {
    return element.toString(16);
});
// ["17", "ff", "7a", "5", "10", "63"]
console.log(hexArray);

// the map() method results in a new
// array rather than modifying the original array. You don’t return a value when using
// forEach() , but you must return a value when using map() .
