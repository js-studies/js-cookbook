// Problem
// You want to find occurrences of a given value in an array, and either remove the element
// or replace with another value.
// Solution
// Use the Array indexOf() and splice() to find and remove/replace array elements:

var animals = new Array("dog","cat","seal","walrus","lion", "cat");

// remove the element from array
animals.splice(animals.indexOf("walrus"), 1);

// The splice() method takes three parameters: the first parameter is required, as it’s the
// index where the splicing is to take place; the second, optional parameter is the number
// of elements to remove; the third parameter, also optional, is a set of the replacement
// elements (if any). If the index is negative, the elements will be spliced from the end rather
// than the beginning of the array:


// If the number of elements to splice is not provided, all elements from the index to the
// end will be removed:
var animals = ["cat","walrus","lion", "cat"];

// remove all elements after second
animals.splice(2); // cat,walrus

console.log(animals.toString());


// ---------------REPLACING
// splice in new element
animals.splice(animals.lastIndexOf("cat"),1,"monkey");

// dog,cat,seal,lion,monkey
console.log(animals.toString());

var animals = ["cat","walrus","lion", "cat"];
// replace second element with two
animals.splice(2,1,"zebra","elephant");
// cat,walrus,zebra,elephant,cat
console.log(animals.toString());

// Using looping and splice to replace and remove elements
var charSets = ["ab","bb","cd","ab","cc","ab","dd","ab"];
// replace element
while (charSets.indexOf("ab") != -1) {
    charSets.splice(charSets.indexOf("ab"),1,"**");
}
// ["**", "bb", "cd", "**", "cc", "**", "dd", "**"]
console.log(charSets);

// delete new element
while(charSets.indexOf("**") != -1) {
    charSets.splice(charSets.indexOf("**"),1);
}
// ["bb", "cd", "cc", "dd"]
console.log(charSets);
