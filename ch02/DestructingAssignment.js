// 2.11. Using a Destructuring Assignment to Simplify Code
// Problem
// You want to assign array element values to several variables, but you 
// really don’t want to have assign each, individually.

var stateValues = [459, 144, 96, 34, 0, 14];
var [Arizona, Missouri, Idaho, Nebraska, Texas, Minesota] = stateValues;
console.log(stateValues);
console.log(Nebraska);

