// Problem
// You want to flatten a two-dimensional array.
// Solution
// Use the Array object concat() method to merge the multidimensional array into a
// single-dimensional array:

var fruitarray = [];
fruitarray[0] = ['strawberry','orange'];
fruitarray[1] = ['lime','peach','banana'];
fruitarray[2] = ['tangerine','apricot'];
fruitarray[3] = ['raspberry','kiwi'];

// flatten array
var newArray = fruitarray.concat.apply([],fruitarray);
// tangerine
console.log(newArray[5]);
