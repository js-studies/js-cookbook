// Problem
// You want to use a function to check an array value, and replace it if it matches a given
// criterion.

var charSets = ["ab","bb","cd","ab","cc","ab","dd","ab"];

function replaceElement(element,index,array) {
    if (element == "ab") array[index] = "**";
}
// apply function to each array element
charSets.forEach(replaceElement);
console.log(charSets); // ["**", "bb", "cd", "**", "cc", "**", "dd", "**"]

//FOREACH in a nodeList
// Problem
// You want to use forEach() on the nodeList returned from a call to querySelector
// All() .
// Solution
// You can coerce forEach() into working with a NodeList (the collection returned by
// querySelectorAll() ) using the following:

// use querySelector to find all second table cells
// var cells = document.querySelectorAll("td + td");
//
// [].forEach.call(cells,function(cell) {
//     sum+=parseFloat(cell.firstChild.data);
// });

// Discussion
// The forEach() is an Array method, and the results of querySelectorAll() is a
// NodeList, which is a different type of object than an Array.
// In the solution, to get forEach() to work with the NodeList, we’re calling the method
// on an empty array, and then using call() on the object to emulate the effect of an Array
// method on the NodeList, as if it were an actual array.
