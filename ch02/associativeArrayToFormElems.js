// Problem
// You want to store form element names and values for later validation purposes.
// Solution
// Use an associative array to store the elements, using the element identifiers as array
// index:

// var elemArray = new Object(); // notice Object, not Array
// var elem = document.forms[0].elements[0];
// elemArray[elem.id] = elem.value;
