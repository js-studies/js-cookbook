// Problem
// You want to extract out a portion of an array but keep the original array intact.
// Solution
// The Array slice() method extracts a shallow copy of a portion of an existing array:

// The Array slice() method extracts a shallow copy of a portion of an existing array:
var animals = ['elephant','tiger','lion','zebra','cat','dog','rabbit','goose'];
var domestic = animals.slice(4,7);
// ['cat','dog','rabbit'];
console.log(domestic);

var mArray = [];
mArray[0] = ['apple','pear'];
mArray[1] = ['strawberry','lemon'];
mArray[2] = ['lime','peach','berry'];
var nArray = mArray.slice(1,2);
console.log(mArray[1]); // ['strawberry','lemon']
nArray[0][0] = 'raspberry';
console.log(nArray[0]); // ['raspberry','lemon']
console.log(mArray[1]); // ['raspberry','lemon']
