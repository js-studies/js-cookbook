// Problem
// You want to search an array for a specific value and get the array element index if found.
// Solution
// Use the Array methods indexOf() and lastIndexOf() :

var animals = new Array("dog","cat","seal","elephant","walrus","lion");
// prints 3
console.log(animals.indexOf("elephant"));

// The indexOf() method returns the first one found, the lastIndex
// Of() returns the last one found:
var animals = new Array("dog","cat","seal","walrus","lion", "cat");
// prints 1
console.log(animals.indexOf("cat"));
// prints 5
console.log(animals.lastIndexOf("cat"));

//Both methods can take a starting index, setting where the search is going to start:
var animals = ["dog","cat","seal","walrus","lion", "cat"];
console.log(animals.indexOf("cat",2)); // prints 5
console.log(animals.lastIndexOf("cat",4)); // prints 1

// ECMAScript 6
// (ES 6) Array method findIndex() , providing a function that tests each array value,
// returning the index of the array element when the test is successful.
// An example use of findIndex() is the following, using the new method to find an array
// element whose value equals or exceeds 100:

console.log('e6');
var nums = [2, 4, 19, 15, 183, 6, 7, 1, 1];
var over = nums.findIndex(function(element) {
    return (element >= 100);
});
console.log(nums[over]);
