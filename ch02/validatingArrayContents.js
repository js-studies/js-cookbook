// Solution
// Use the Array every() method to check that EVERY element passes a given criterion. For
// instance, the following code checks to ensure that every element in the array consists
// of alphabetical characters:


 //EVERY element passes a given criterion

//testing function
function testValue (element, index, array) {
    var textExp = /^[a-zA-Z]+$/;
    return textExp.test(element);
}

var elemSet = ["**",123,"aaa","abc","-",46,"AAA"];

// run test
var result = elemSet.every(testValue);
//false;
console.log(result);

var elemSet2 = ["elephant","lion","cat","dog", "AAA", "AA", "asd"];
result = elemSet2.every(testValue);
// true
console.log(result);

//ONE or MORE of the elements pass the
// criteria.
var result = elemSet.some(testValue);
console.log(result); // true
// Unlike the Array methods I covered earlier in the chapter, every() and some() functions
// do not work against all array elements: they only process as many array elements as
// necessary to fulfill their functionality.
